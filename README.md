# utrigger

A service built with [pyudev](https://pyudev.readthedocs.io/en/latest/) that executes a command when a particular device (by UUID) appears. The idea is that you can use this script to, for example, sync a directory to a USB drive or launch any other program when a drive is inserted.

## A Note About Security

Please be mindful of what you are doing with this program. Devices that attach to your computer via USB, or whatever, are extremely difficult, if not impossible, to secure. For example, below I tend to use UUIDs but there is nothing stopping someone from making a byzantine USB drive that clones my UUID and tricks this system into treating it like my normal USB drive. Then all they have to do is plug it into my computer and wait for my configured command to copy sensitive files onto their USB drive (if my utrigger configuration is set to do that).

## Setup

### Installation

Simply clone from git to your preferred directory: `git clone git@gitlab.com:abraxos/utrigger.git` and then go into that repository directory and install using pip: `pip install .`. If you plan to be changing the code, add the `-e` parameter to install "in place".

You can verify that everything is installed correctly by executing `utrigger --help`. Given that you most likely want `utrigger` to run on startup, please refer to the **Running on Startup** section to see how one might configure it to do so.

### Configuration

You can configure `utrigger` with the `utrigger edit` command, which will automatically edit the `~/.config/utrigger` file in a safe way that will attempt to prevent configuration errors. It is _not_ recommended that you edit the file manually as the resulting behavior is undefined. The `edit` command exists to help you edit the configuration safely. Note that you do not need to restart the utrigger service, it will automatically pick up the new configuration.

Example configuration file with comments on each config value:

```
triggers:
  - trigger_property: ID_FS_UUID # You can find a list of keys here: https://pyudev.readthedocs.io/en/latest/api/pyudev.html#pyudev.Device
    value: 7c99e267-9624-4469-9455-51b345401af6 # You can find the UUID of a partition with the blkid command
    actions: [add, change, remove] # valid values: add, change, remove
    command: echo 'Hooray!'
  - trigger_property: ...
    value: ...
    actions: [...]
    command: ...
editor: emacs # if you don't set this, it will ask you every time you edit
```

The example above will print "Hooray!" every time a specific USB Drive is added, changed, or removed.

You need some understanding of how udev works. In the above example I am using UUIDs, you can use whatever property of a device that you like. For example, UUID's only exist on linux file systems like Ext4 or BTRFS (as far as I know) so if you have a FAT32 USB Drive you would need to use the file system label or something like that.

### Running on Startup

`utrigger` is a pretty simple script and needs to be run by some kind of other service management system like supervisor or systemd in order to start `utrigger` on startup (nothing is stopping you from running it manually if you so choose). The following is an example of what you could add to your [supervisord configuration file](http://supervisord.org/configuration.html) in order to run `utrigger` on startup and restart it if it crashes:

```
[program:utrigger]
command=/usr/local/bin/utrigger run
process_name=%(program_name)s
numprocs=1
autostart=true
autorestart=unexpected
startsecs=3
startretries=3
user=root
redirect_stderr=true
stdout_logfile=/var/log/utrigger/stdout.log
stdout_logfile_maxbytes=50MB
stdout_logfile_backups=10
```

_Note that you need to make sure that you're pointing to the correct executable (in case you're using a virtualenv), that `/var/log/utrigger/stdout.log` exists and that the user (in this case `root`) can write to it._

For what its worth, supervisord is pretty easy to install on Ubuntu/Debian with `apt-get install supervisor`, then you can edit the `/etc/supervisor/supervisord.conf` file to add the above, and then restart with `sudo service supervisor restart` to activate the new configuration which would start the daemon. There is more information available here: http://supervisord.org/.