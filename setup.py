"""Setup file for a template python client-authenticated webapp."""
from setuptools import setup, find_packages

with open('README.md') as f:
    README = f.read()

with open('requirements.txt') as f:
    REQUIREMENTS = f.readlines()

setup(
    name='utrigger',
    version='0.1.0',
    description="A system for monitoring for udev events and executing commands in response to them.",
    long_description=README,
    author='Eugene Kovalev',
    author_email='euge.kovalev@gmail.com',
    url='https://gitlab.com/abraxos/utrigger',
    packages=find_packages(exclude=('tests', 'docs')),
    install_requires=REQUIREMENTS,
    entry_points={'console_scripts': ['utrigger=utrigger:cli']},
)