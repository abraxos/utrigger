from pprint import pformat
from os import system, environ
from pathlib import Path
from typing import List, Union, Optional
from enum import Enum
from shlex import split
from shutil import which
from traceback import format_exc
import logging as log
import subprocess
from subprocess import CalledProcessError
from tempfile import NamedTemporaryFile
import webbrowser

from pyudev import Context, Monitor, Device
from pydantic import BaseModel, ValidationError, validator
from typeguard import typechecked
import click
import yaml


DEFAULT_CONFIG_PATH = Path.home() / '.config' / 'utrigger.yaml'
RETRY = {'r', 'retry', 'ret'}
DISCARD = {'d', 'discard', 'dis'}

# Exit Codes
SUCCESS = 0
CONFIG_NOT_FOUND = 1
USER_EXIT = 2


# setup logging
log.basicConfig(format='%(levelname)s - %(module)s.%(funcName)s - [%(asctime)s]: %(message)s')
log.getLogger().setLevel(log.DEBUG)


class Action(str, Enum):
    add = 'add'
    change = 'change'
    remove = 'remove'


class TriggerConfiguration(BaseModel):
    trigger_property: str
    value: str
    actions: List[Action]
    command: str # Validator converts to list of strings

    @validator('command')
    def valid_shell_cmd(cls, value) -> List[str]:
        try:
            return split(value)
        except BaseException:
            raise ValueError('Cannot parse shell command: `{}`')


class uTriggerConfiguration(BaseModel):
    triggers: List[TriggerConfiguration]
    editor: Optional[str]


@typechecked
def load_config(config_path: Path) -> uTriggerConfiguration:
    return uTriggerConfiguration(**yaml.load(config_path.open('r')))


@typechecked
def get_editor_from_config(config_path: Path) -> Optional[str]:
    editor: Optional[str] = None
    try:
        editor = load_config(config_path).editor
    except BaseException:
        log.debug("Could not load editor from configuration: {}".format(config_path))
    return editor


@typechecked
def log_event(device: Device):
    log.info('{}: {}'.format(device.action, device))


@typechecked
def run_monitor(config_path: Path):
    monitor = Monitor.from_netlink(Context())
    monitor.filter_by(subsystem='block')

    for device in iter(monitor.poll, None):
        try:
            log_event(device)
            config: uTriggerConfiguration = load_config(config_path)
            for trigger in config.triggers:
                if trigger.trigger_property not in device:
                    log.debug("Property {} is not available for device: {}, ignoring".format(trigger.trigger_property,
                                               device))
                else:
                    if trigger.value != device.get(trigger.trigger_property):
                        log.debug("Value [{}:{}] does not match device: [{}:{}], ignoring"
                                  .format(trigger.trigger_property,
                                          trigger.value,
                                          trigger.trigger_property,
                                          device.get(trigger.trigger_property)))
                    else:
                        if str(device.action) not in [a.value for a in trigger.actions]:
                            log.debug("Action does not match {} =/= {}, ignoring".format(device.action,
                                                       trigger.actions))
                        else:
                            log.info("Trigger match:\n\tProperty: {}\n\tValue: {}\n\tAction: {}"
                                     .format(trigger.trigger_property,
                                             trigger.value,
                                             device.action))
                            log.info("Executing: {}".format(trigger.command))
                            try:
                                subprocess.run(trigger.command, check=True)
                                log.info("Trigger execution complete.")
                            except CalledProcessError as e:
                                log.warning(e)
        except (ValidationError, yaml.YAMLError) as e:
            log.error("Error while parsing configuration at `{}`: {}"\
                      .format(config_path, e))
        except BaseException as e:
            log.error("Cannot process event: {}\n\tStack Trace:\n  {}"
                      .format(str(e), format_exc()))


@typechecked
def ensure_editor_is_set_or_exit(editor: Optional[str]) -> str:
    if editor is None or not which(editor):
        new_editor: str = input("Please enter executable to use as an editor (e.g. emacs, vim, nano..., Ctrl+C to exit): ")
        editor = ensure_editor_is_set_or_exit(new_editor)
    return editor


@typechecked
def edit_config(editor: str, config_path: Path) -> None:
    system("{} {}".format(editor, config_path))


@typechecked
def prompt_user_to_edit_until_correct_or_exit(editor: str, config_path: Path) -> None:
    edit_config(editor, config_path)
    correct = False
    while not correct:
        try:
            load_config(config_path)
            correct = True
        except (ValidationError, TypeError, yaml.YAMLError) as e:
            valid = False
            while not valid:
                log.warning(e)
                result = input("Configuration invalid, would you like to retry the edit or discard your changes entirely? (r/d): ")
                if result.lower() in RETRY:
                    edit_config(editor, config_path)
                    valid = True
                elif result.lower() in DISCARD:
                    exit(USER_EXIT)
                else:
                    valid = False


@click.group()
def cli():
    """A system for monitoring for udev events and executing commands in response to them."""
    pass


@cli.command()
@click.option('-c', '--config-path', default=DEFAULT_CONFIG_PATH,
              help='Path to the configuration file, defaults to \
                    ~/.config/utrigger.yaml')
def edit(config_path: Union[str, Path]):
    """Create or edit the configuration file"""
    # using a temp file as a context, copy from ~/.config/utrigger if it exists
    config_path: Path = Path(config_path)
    if not config_path.exists():
        log.info("Configuration path not found, creating: {}".format(config_path))
        config_path.parent.mkdir(parents=True, exist_ok=True)
        config_path.touch(exist_ok=True)
        config_path.chmod(0o400)
    with NamedTemporaryFile() as fp:
        temp_path = Path(fp.name)
        temp_path.open('w').write(config_path.open().read())
        editor = ensure_editor_is_set_or_exit(get_editor_from_config(config_path))
        prompt_user_to_edit_until_correct_or_exit(editor, temp_path)
        config_path.chmod(0o600)
        config_path.open('w').write(temp_path.open().read())
        config_path.chmod(0o400)


@cli.command()
@click.option('-c', '--config-path', default=DEFAULT_CONFIG_PATH,
              help='Path to the configuration file, defaults to \
                    ~/.config/utrigger.yaml')
def run(config_path: Union[str, Path]):
    """Runs the utrigger monitor and reacts to udev disk events as configured
       in the configuration file, defaults to: ~/.config/utrigger.yaml"""
    # Check that the config file exists, if not, error and exit
    config_path: Path = Path(config_path)
    if not config_path.exists():
        log.critical("Configuration file not found: {}".format(config_path))
        exit(CONFIG_NOT_FOUND)
    # Every time there is an event, re-parse the configuration, and act accordingly
    log.debug("Configuration exists at: {}".format(config_path))
    run_monitor(config_path)


if __name__ == '__main__':
    cli()